#!/bin/bash

# ----------------------------------------------------------------------------------------------------
# Welcome to the script for disk space analysis 
# The base of the script is the command 'du' (don't display symbolic links in the list)
# (the variables of the script are all in global, to be able to use them easily in the functions)
# 
# And you can add some options display more infomation :
# -d <path>  : will target a different directory than the current one
# -h 		 : will display the sizes in a human readable way (KB, MB, GB)
# -s 		 : will sort the results in descending order of used disk space 
# -r <regex> : will only target items matching the regular expression passed as a parameter
# -f 		 : will also display files of the trageted folder, in addition to its direct subfolders
# -a 		 : will also display hidden files and folders
# -o <file>  : will return the result in a file, indicating the date and time the script was run
# -g 		 : will display a progress bar, visually representing the space used by each element
# 
# ----------------------------------------------------------------------------------------------------


# Definitions of the functions
# ----------------------------------------------------------------------------------------------------

# Function to check the actions to be applied to the command 'du'
function check_actions()
{
	if $sorting 
	then
		actions="$actions | sort -rh" # add sorting in descending order of used disk space
	fi
	
	if [ -n "$exp_reg" ] # if the string is not empty
	then
		actions="$actions | grep -E $exp_reg" # add matching the regular expression
	fi
	
	if $del_hidden 
	then
		actions="$actions | grep -v -E /\\\." # add deleting hidden folders and hidden files
											  # hidden files and folders begin with .
	fi
}

# Function to delete bad lines and format lines
function format_lines()
{
	IFS=$'\n' # set the separator at the line break 
	for line in $cmd # in each line, one folder/file with its size
	do
		graphic=""
		progress_bar # return the progess bar of the folder/file in $line
		
		if [ ! -L $name ] # if it's not a symbolic link
		then
			if ! $optf && [ ! -f $name ] # if the -f option is not selected, files are not displayed 
			then
				res_end="$res_end \n$line $graphic"
			elif $optf # if the -f option is selected, folders and files are displayed 
			then
				res_end="$res_end \n$line $graphic"
			fi
		fi
	done
}

# Function to return the progress bar of the file/folder
function progress_bar()
{ 
	lenght_tot=20 #lenght total of the progress bar
	
	#get the name and the size of the current file/folder
	name=`echo $line | cut -f2`
	size=`echo $line | cut -f1`
	
	if $optg # if the -g option is selected, the progress bar is not empty
	then
		lenght=$(echo "$lenght_tot*$size/$size_tot" | bc)
		graphic="["
		
		for (( i=0 ; i<$lenght ; i++ )) # filling the bar with '#'
		do
			graphic="$graphic#"
		done
		
		void=$(echo "$lenght_tot-$lenght" | bc)
		for (( i=0 ; i<$void ; i++ )) # filling the rest of the bar with '-'
		do
			graphic="$graphic-"
		done
		graphic="$graphic]"
	fi
}

# Function to display the result of the script
function display()
{
	# the -e option in the 'echo' command, takes into account the '\n' caracter

	if [ -n "$write_file" ] # if the string is not empty, have to write the result in a file 
	then
		echo `date +"%A %d %B %Y %T"` >> $write_file # write the date and time
		echo $pwd_res >> $write_file
		( echo $header;  echo -e $res_end ) | awk '{ print $2, $1, $3 }' | column -t >> $write_file
		echo "" >> $write_file
		
	else # or else, have to write the result in the console 
		echo $pwd_res
		( echo $header;  echo -e $res_end ) | awk '{ print $2, $1, $3 }' | column -t
	fi
}


# Starting script
# ----------------------------------------------------------------------------------------------------

options="" # the list of options add to the 'du' command

sorting=false # sort or not the result
del_hidden=true # delete hidden files/folders or not

exp_reg="" # regular expression
other_folder="" # execute the command in a different directory
write_file="" # write the result in a file

optf=false # -f option or not
optg=false # -g option or not

# Check the options passed with the script
while getopts "d: h s r: f a o: g" option
do
	case $option in 
		d)
			options="$options $OPTARG"
			other_folder="$OPTARG"
			;;
		h)
			options="$options -$option"
			;;
		s)
			sorting=true
			;;
		r)
			exp_reg="$OPTARG"
			;;
		f)
			optf=true
			;;
		a)
			del_hidden=false
			;;
		o)
			write_file="$OPTARG"
			;;
		g)
			optg=true
			;;
		?)
			;;
	esac
done

# Check actions to be applied to the command 'du'
actions=""
check_actions 

# Execute the 'du' command with options and actions
# By default, -a option is add and will delete bad lines after
temp="du -d1 -a $options $actions"
cmd=`eval $temp`

# Size of the directory
size_tot=`du -d0 $other_folder | cut -f1`

# Delete bad lines
res_end=""
format_lines

# Display the result
pwd_res=`pwd`
header="Size Folder"
display
